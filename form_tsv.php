<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="./css/style.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
</head>
<style><?php include './css/style.css' ?></style>
<body>
    
<?php
	


	$name_student = $admission_date = $gender = $department = $student_address ='';
	$errors = array('name_student' => '', 'gender' => '', 'department' => '', 'admission_date' => '');
 
    //IMAGE
    $checkupload = 1;
    $target_dir = "upload/";


	if (isset($_POST['submit'])) {

		//check name of student
		if (empty($_POST['name_student'])) {
			$errors['name_student'] = 'Hãy nhập tên <br />';
		} else {
			$name_student = $_POST['name_student'];
		}

		//check gender of student
		if (empty($_POST['gender'])) {
			$errors['gender'] = 'Hãy chọn giới tính <br />';
		} else {
			$gender = $_POST['gender'];
		}

		//check department of student
		if (empty( $_POST['department'])) {
			$errors['department'] = 'Hãy chọn phân khoa <br />';
		} else {
			$department = $_POST['department'];
		}

		//check year of birth
		if (empty($_POST['admission_date'])) {
			$errors['admission_date'] = 'Hãy nhập ngày tháng năm sinh <br />';
		} elseif (!preg_match('/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/', $_POST['admission_date'])) {
            $errors['admission_date'] = 'Nhập ngày tháng năm sinh đúng định dạng <br />';
        } else {
		    $admission_date = $_POST['admission_date'];
		}

        $student_address = $_POST['student_address'];   
        
        // upload image to folder upload
        date_default_timezone_set("Asia/Ho_Chi_Minh");
        if ($_FILES["student_image"]["size"] > 0 ) {
            $check = getimagesize($_FILES["student_image"]["tmp_name"]);
            if($check !== false) {
                $checkupload = 1;

                // tạo thư mục upload
                if (!is_dir($target_dir)) {
                    mkdir($target_dir, 0777, true);
                }
                // đổi tên file
                $timestemp = explode(".", $_FILES['student_image']["name"]);
                $_FILES["student_image"]["name"] = $timestemp[0]."_".date("YmdHis").".".$timestemp[1];

                $target_file = $target_dir . basename($_FILES["student_image"]["name"]);
                if (move_uploaded_file($_FILES["student_image"]["tmp_name"], $target_file)) {
                    $_POST["student_image"] = 'upload/'.$_FILES["student_image"]["name"];
                } else {
                    $_POST["student_image"] = "Error";
                }
            } else {
                $checkupload = 0;
                $errors['student_image'] = "File không đúng định dạng";
            }
        }

        $_SESSION["data"] = $_POST;
        $_SESSION["errors"] = $errors;

		if (array_filter($errors)) {
			///
		} else {
			header("Location: form_tsv_do.php?name_student=$name_student&gender=$gender&department=$department&admission_date=$admission_date&student_address=$student_address");
			echo 'test bug';
		}

	}

	?>

<!-- --------------------------------------------------------------- -->

    <div class="center">
        <div class="container">
        <form class="err" name='formSignUp' action='form_tsv.php' method="POST" >
            
            <?php
				echo $errors['name_student'];
				echo $errors['admission_date'];
				echo $errors['gender'];
				echo $errors['department'];
			?>
            
        </form>
        <form name='formSignUp' action='form_tsv.php' method="POST" enctype="multipart/form-data">
                <div class="border_box">
                    <span class="label red">Họ và tên</span>
                    <input class="input" type="text" name="name_student" value="<?php echo $name_student ?>">
                </div>
                <div class="border_box">
                    <span class="label red">Giới tính</span>
                    <div class="gender">
                        <div class="check">
                        <?php 
                        $listGender =array(
                           2 => 'Nam',
                           1 => 'Nữ',
                        );
                        $_SESSION["listGender"] = $listGender;
                            foreach ($listGender as $key => $value) {
                                echo "<div class='gender'>
                                    <input type='radio' name='gender' id='gender' value=".$key.">
                                    <label>".$value."</label>
                                </div>";
                            }

                        ?>
                        </div>
                    </div>
                </div>
                <div class="border_box">
                    <span class="label red">Phân khoa</span>
                    <select class="input" name='department'>
                        <?php
                            $department = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
                            foreach ($department as $key => $value) {
                                echo '<option value="' . $key . '">' . $value . '</option>';
                            }
                        ?>
                    </select>
                </div>

                <div class="border_box">
                    <span class="label red">Ngày sinh</span>
                    <input class="input" type="date" name="admission_date" id="admission date" class="form-control" value="<?php echo $admission_date ?>">
                </div>
                <div class="border_box">
                    <span class="label">Địa chỉ</span>
                    <input class="input" type="text" name="student_address" value="<?php echo $student_address ?>">
                </div>
                <div class="border_box">
                    <span class="label">Hình ảnh</span>
                    <input class="input " style="border: none; verticle-align: middle" type="file" accept="image/png, image/gif, image/jpeg" name="student_image" id="student_image">
                </div>
                <div class="login-btn">
                    <button class="button_submit" type="submit" name="submit">Đăng nhập</button>
                </div>
            </form>
        </div>
    </div>
</body>
</html>